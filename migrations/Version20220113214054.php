<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220113214054 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void {

        $this->addSql('CREATE TABLE bike (id INT AUTO_INCREMENT NOT NULL, type_bike_id INT NOT NULL, code VARCHAR(20) NOT NULL, details VARCHAR(255) DEFAULT NULL, INDEX IDX_4CBC3780FD85EE9A (type_bike_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_bike (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, premium TINYINT(1) NOT NULL, days_basic INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, lastname VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL COMMENT \'The same as the email\', password VARCHAR(60) NOT NULL, roles JSON NOT NULL, token VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_bike (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, bike_id INT NOT NULL, rent_start DATE NOT NULL, rent_end DATE NOT NULL, return_bike DATE DEFAULT NULL, rent_cost NUMERIC(10, 0) DEFAULT NULL, INDEX IDX_363D7B49A76ED395 (user_id), INDEX IDX_363D7B49D5A4816F (bike_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bike ADD CONSTRAINT FK_4CBC3780FD85EE9A FOREIGN KEY (type_bike_id) REFERENCES type_bike (id)');
        $this->addSql('ALTER TABLE user_bike ADD CONSTRAINT FK_363D7B49A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_bike ADD CONSTRAINT FK_363D7B49D5A4816F FOREIGN KEY (bike_id) REFERENCES bike (id)');
        $this->addSql('ALTER TABLE user ADD bonus_point INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_bike CHANGE rent_end rent_end DATE DEFAULT NULL');

        $this->addSql('ALTER TABLE user CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE user_bike CHANGE rent_end rent_end DATE NOT NULL');
        $this->addSql('ALTER TABLE user_bike ADD returned TINYINT(1) NOT NULL DEFAULT 0');

        $this->addSql('INSERT INTO type_bike (name, premium, days_basic) VALUES (\'electric\', 1, 1)');
        $this->addSql('INSERT INTO type_bike (name, premium, days_basic) VALUES (\'electric\', 0, 1)');
        $this->addSql('INSERT INTO type_bike (name, premium, days_basic) VALUES (\'normal\', 1, 3)');
        $this->addSql('INSERT INTO type_bike (name, premium, days_basic) VALUES (\'normal\', 0, 3)');
        $this->addSql('INSERT INTO type_bike (name, premium, days_basic) VALUES (\'vitage\', 1, 5)');
        $this->addSql('INSERT INTO type_bike (name, premium, days_basic) VALUES (\'vitage\', 0, 5)');

        $this->addSql('INSERT INTO bike (code, details, type_bike_id) VALUES (\'001\', \'perfect conditions\', 1)');
        $this->addSql('INSERT INTO bike (code, details, type_bike_id) VALUES (\'002\', \'basic conditions\', 2)');
        $this->addSql('INSERT INTO bike (code, details, type_bike_id) VALUES (\'003\', \'perfect conditions\', 3)');
        $this->addSql('INSERT INTO bike (code, details, type_bike_id) VALUES (\'004\', \'basic conditions\', 4)');
        $this->addSql('INSERT INTO bike (code, details, type_bike_id) VALUES (\'005\', \'perfect conditions\', 5)');
        $this->addSql('INSERT INTO bike (code, details, type_bike_id) VALUES (\'006\', \'basic conditions\', 6)');

        $this->addSql('INSERT INTO user (name, lastname, email, username, password, roles, created_at) VALUES (\'Leonel\', \'Messi\', \'leones.messi@rentalbike.com\', \'leones.messi@rentalbike.com\', \'$2y$10$YyzxqP6BE3PZ4wtpNUVhEugGgL2eldkBaMmaR5l2GeAD0gHbBT5H6\', \'[]\', Now() )');
        $this->addSql('INSERT INTO user (name, lastname, email, username, password, roles, created_at) VALUES (\'Andres\', \'Iniesta\', \'andres.iniesta@rentalbike.com\', \'andres.iniesta@rentalbike.com\', \'$2y$10$YyzxqP6BE3PZ4wtpNUVhEugGgL2eldkBaMmaR5l2GeAD0gHbBT5H6\', \'[]\', Now() )');
    }

    public function down(Schema $schema) : void {

        $this->addSql('ALTER TABLE user_bike DROP FOREIGN KEY FK_363D7B49D5A4816F');
        $this->addSql('ALTER TABLE bike DROP FOREIGN KEY FK_4CBC3780FD85EE9A');
        $this->addSql('ALTER TABLE user_bike DROP FOREIGN KEY FK_363D7B49A76ED395');
        $this->addSql('DROP TABLE bike');
        $this->addSql('DROP TABLE type_bike');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_bike');
    }
}