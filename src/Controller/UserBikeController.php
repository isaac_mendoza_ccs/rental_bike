<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Bike;
use App\Service\Helpers;
use App\Service\jwtAuth;
use App\Entity\UserBike;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Doctrine\Persistence\ManagerRegistry;

/**
 * @Route("/user/bike")
 */
class UserBikeController extends AbstractController {

    /**
     * @Route("", name="user_bike_create", methods={"POST", "OPTIONS"} )
     */
    public function user_bike_create(Request $request, jwtAuth $jwtAuth){

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                return  $this->forward(
                    'App\Controller\DefaultController::create',
                    ['forward' => '{"UserBike":'.$request->request->get('data').'}' ]
                );
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/{id}", name="user_bike_read", methods={"GET"} )
     */
    public function user_bike_read(Request $request, jwtAuth $jwtAuth, $id){

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                return $this->forward(
                    'App\Controller\DefaultController::read',
                    ['forward' => ['entity' => 'UserBike', 'parameters' => ['user_bike|id' => '=|'.$id] ] ]
                );
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/{id}", name="user_bike_update", methods={"PUT"} )
     */
    public function user_bike_update(Request $request, jwtAuth $jwtAuth, $id){

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                return $this->forward(
                    'App\Controller\DefaultController::update',
                    ['forward' => ['entity' => 'UserBike', 'id' => $id, 'parameters' => $request->request->get('data') ] ]
                );
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/{id}", name="user_bike_delete", methods={"DELETE"} )
     */
    public function user_bike_delete(Request $request, jwtAuth $jwtAuth, $id){

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                return  $this->forward(
                    'App\Controller\DefaultController::delete',
                    ['forward' => ['entity' => 'UserBike', 'id' => $id] ]
                );
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("", name="user_bike_list", methods={"GET"} )
     */
    public function user_bike_list(Request $request, jwtAuth $jwtAuth) {

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                return  $this->forward(
                    'App\Controller\DefaultController::read',
                    ['forward' => ['entity' => 'UserBike', 'parameters' => $request->query->all() ]]
                );
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/rent", name="user_bike_rent", methods={"POST", "OPTIONS"} )
     */
    public function user_bike_rent(ManagerRegistry $doctrine, Request $request, jwtAuth $jwtAuth, Helpers $helpers){

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/

                $em = $doctrine->getManager();
                $params = json_decode($request->request->get('data'), 1);

                $query = $em->createQuery(
                    'SELECT ub
                    FROM App\Entity\UserBike ub
                    WHERE ub.bike = :bike
                    ORDER BY ub.id'
                )->setParameter('bike', $params['bike']);
                
                $bike = $query->getSingleResult();
                
                if($bike && !$bike->getReturned()){
                    return $helpers->json([
                        'ok' => false,
                        'message' => 'the bike is already rented!'
                    ], 400);
                }
                
                $user = $doctrine->getRepository(User::class)->find($params['user']);
                $userBike = new UserBike();
                $userBike->setUser($user);
                $userBike->setBike($bike->getBike());
                $userBike->setRentStart( new \DateTime() );
                $em->persist($userBike);
                $em->flush();

                return $helpers->json([
                    'ok' => true,
                    'message' => 'Updated!',
                    'data' => $bike
                ], 200);

            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/{id}/rent/return", name="user_bike_return", methods={"POST", "OPTIONS"} )
     */
    public function user_bike_return(ManagerRegistry $doctrine, Request $request, jwtAuth $jwtAuth, Helpers $helpers, $id){
        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                $em = $doctrine->getManager();
                $userBike = $doctrine->getRepository(UserBike::class)->find($id);
                $params = json_decode($request->request->get('data'), 1);
                $userBike->setReturnBike( \DateTime::createFromFormat('Y-m-d h:i:s', date('Y-m-d h:i:s', strtotime(str_replace('-', '/', $params['return_bike']. ' 00:00:00')))) );
                $bike = $userBike->getBike();
        
                // $userBike->getRentEnd()
                $rentStart = $userBike->getRentStart();
                $basicDays = $bike->getTypeBike()->getDaysBasic();
                $rentEnd = \DateTime::createFromFormat('Y-m-d h:i:s',  date("Y-m-d h:i:s",strtotime($rentStart->format('Y-m-d')."+ $basicDays days")) ); 
                $returned = $userBike->getReturnBike();
                if( $returned > $rentEnd){
                    $diffDays = date_diff( $rentEnd, $returned );
                    $diffDays = $diffDays->days;
                }else{
                    $diffDays = 0 ;
                }
        
                $premium = $bike->getTypeBike()->getPremium();
                $penaltyCost = $premium ? 30 : 10;
                $penalty = ($diffDays + 1) * $penaltyCost;
        
                return new JsonResponse([
                    'premium' => $premium,
                    'basicDays' => $basicDays,
                    'rentStart' => $rentStart,
                    'tentativeReturn' => $rentEnd,
                    'returned' => $returned,
                    'diffDays' => $diffDays,
                    'normalPay' => $penaltyCost,
                    'penaltyPay' => ($penalty - $penaltyCost),
                    'totalPay' => $penalty 
                ]);
        
                $em->persist($userBike);
                $em->flush();
        
                return $helpers->json([
                    'ok' => true,
                    'message' => 'Updated!',
                    'data' => $userBike
                ], 200);
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

}