<?php

namespace App\Controller;

use App\Service\jwtAuth;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/bike")
 */
class BikeController extends AbstractController {

    /**
     * @Route("", name="bike_create", methods={"POST", "OPTIONS"} )
     */
    public function bike_create(Request $request, jwtAuth $jwtAuth){

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                return  $this->forward(
                    'App\Controller\DefaultController::create',
                    ['forward' => '{"Bike":'.$request->request->get('data').'}' ]
                );
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/{id}", name="bike_read", methods={"GET"} )
     */
    public function bike_read(Request $request, jwtAuth $jwtAuth, $id){

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                return $this->forward(
                    'App\Controller\DefaultController::read',
                    ['forward' => ['entity' => 'Bike', 'parameters' => ['bike|id' => '=|'.$id] ] ]
                );
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/{id}", name="bike_update", methods={"PUT"} )
     */
    public function bike_update(Request $request, jwtAuth $jwtAuth, $id){

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                return $this->forward(
                    'App\Controller\DefaultController::update',
                    ['forward' => ['entity' => 'Bike', 'id' => $id, 'parameters' => $request->request->get('data') ] ]
                );
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/{id}", name="bike_delete", methods={"DELETE"} )
     */
    public function bike_delete(Request $request, jwtAuth $jwtAuth, $id){

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                return  $this->forward(
                    'App\Controller\DefaultController::delete',
                    ['forward' => ['entity' => 'Bike', 'id' => $id] ]
                );
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("", name="bike_list", methods={"GET"} )
     */
    public function bike_list(Request $request, jwtAuth $jwtAuth) {

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                return  $this->forward(
                    'App\Controller\DefaultController::read',
                    ['forward' => ['entity' => 'Bike', 'parameters' => $request->query->all() ]]
                );
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }
}