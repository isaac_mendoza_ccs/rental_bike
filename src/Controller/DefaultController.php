<?php

namespace App\Controller;

use App\Service\Helpers;
use App\Service\MysqlEasy;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/")
 */
class DefaultController extends AbstractController {

    /**
     * @Route("/api/doc", name="swagger_ui")
     */
    public function index() {

        $yamlFile = __DIR__ . '/../../templates/swagger_ui/swagger.yaml';
        $viewData = [
            'spec' =>json_encode(Yaml::parseFile($yamlFile)),
        ];
        return $this->render('swagger_ui/swagger.twig', $viewData);
    }

    /**
     * @Route("/mysql-easy/read", name="read", methods={"GET", "OPTIONS"} )
     */
    public function read(Request $request, MysqlEasy $mysqlEasy, $forward = null){

        try {
            if($forward){
                //get parameters
                $parameters = $forward['parameters'];
                $entity = $forward['entity'];
                $ignored = isset($forward['ignored']) ? $forward['ignored'] : [];
                // select data
                $data = $mysqlEasy->select( $entity, $parameters, $ignored );
                return new JsonResponse( array_merge(['ok' => true, 'message' => 'Success!'], $data) );
            }else{
                return new JsonResponse(['message' => '¡Not authorized!', 'ok' => false], 403);
            }
        } catch (Exception $e) {
            return new JsonResponse(['message' => '¡Operation failed! ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/mysql-easy/create", name="create", methods={"POST", "OPTIONS"} )
     */
    public function create(Request $request, MysqlEasy $mysqlEasy, Helpers $helpers, $forward = null){

        try {
            if($forward){
                //get parameters
                $parameters = $forward;
                // select data
                $data = $mysqlEasy->insert($parameters);
                // return success
                return $helpers->json([
                    'ok' => true,
                    'message' => '¡Created!',
                    'data' => $data->getId()
                ], 201);
            }else{
                return new JsonResponse(['message' => '¡Not authorized!', 'ok' => false], 403);
            }
        } catch (Exception $e) {
            return new JsonResponse(['message' => '¡Operation failed! ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/mysql-easy/update", name="update", methods={"PUT", "OPTIONS"} )
     */
    public function update(Request $request, MysqlEasy $mysqlEasy, Helpers $helpers, $forward = null){

        try {
            if($forward){
                //get parameters
                $id = $forward['id'];
                $parameters = $forward['parameters'];
                $entity = $forward['entity'];
                //build obj to update
                $data = [$entity => array_merge(['id' => $id], json_decode($parameters, 1) ) ];
                // select data
                $entity = $mysqlEasy->insert( $data );
                // return success
                return $helpers->json([
                    'ok' => true,
                    'message' => 'Updated!',
                    'data' => $entity->getId()
                ], 200);
            }else{
                return new JsonResponse(['message' => '¡Not authorized!', 'ok' => false], 403);
            }
        } catch (Exception $e) {
            return new JsonResponse(['message' => '¡Operation failed! ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/mysql-easy/delete", name="delete", methods={"DELETE", "OPTIONS"} )
     */
    public function delete(Request $request, MysqlEasy $mysqlEasy, $forward = null){

        try {
            if($forward){
                //get parameters
                $parameters = $forward;
                // select data
                $data = $mysqlEasy->removeEntityByNameId( $parameters['entity'], $parameters['id'] );
                return new JsonResponse(['ok' => false, 'message' => 'Deleted!', 'data' => null], 200 );
            }else{
                return new JsonResponse(['message' => '¡Not authorized!', 'ok' => false], 403);
            }
        } catch (Exception $e) {
            return new JsonResponse(['message' => '¡Operation failed! ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }
}