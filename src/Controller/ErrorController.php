<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ErrorController extends AbstractController{

    /**
     * @Route("/error", name="error")
     */
    public function show(Request $request) {

        $statusCode = method_exists($request->get('exception'), 'getStatusCode') === true ? $request->get('exception')->getStatusCode() : 400;
        $message = explode("()\" ", $request->get('exception')->getMessage());
        $message = count( $message ) > 1 ? $message[1] : $message[0];
        return new JsonResponse([
            'ok' => false,
            'message' => $message,
        ], $statusCode );
    }
}