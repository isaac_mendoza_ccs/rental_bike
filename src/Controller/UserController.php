<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\Helpers;
use App\Service\jwtAuth;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/user")
 */
class UserController extends AbstractController {

    /**
     * @Route("/login", name="user_login", methods={"POST"} )
     */
    public function login(Request $request, Helpers $helpers, jwtAuth $jwtAuth) {

        try {
            $parameters = $request->request->all();
            $user = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(['email' => $parameters['email']]);
            if ($user) { // return user
                $token = $jwtAuth->signUp($user);
                $user->setToken($token);
                $user = $helpers->saveEntity($user);
                $user->setPassword('encripted');
                return $helpers->json(array('data' => $user, 'ok' => true), 200);
            }
            return new JsonResponse(['message' => 'You could not be authenticated', 'ok' => false], 403);
        } catch (\Exception $ex) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage(), 'ok' => false], 500);
        }
    }

    /**
     * @Route("", name="user_create", methods={"POST", "OPTIONS"} )
     */
   public function user_create(Request $request, jwtAuth $jwtAuth){

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                return  $this->forward(
                    'App\Controller\DefaultController::create',
                    ['forward' => '{"User":'.$request->request->get('data').'}' ]
                );
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/{id}", name="user_read", methods={"GET"} )
     */
    public function user_read(Request $request, Helpers $helpers, jwtAuth $jwtAuth, $id){

        try {
            $user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {
                $data = json_decode($this->forward(
                    'App\Controller\DefaultController::read',
                    ['forward' => ['entity' => 'User', 'parameters' => ['user|id' => '=|'.$id] ] ]
                )->getContent(), 1);
                $user = $data['data'][0];
                $user['password'] = 'encripted';
                $user['token'] = 'encripted';
                $data['data'] = [$user];
                return $helpers->json($data);
            }
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/{id}", name="user_update", methods={"PUT"} )
     */
    public function user_update(Request $request, jwtAuth $jwtAuth, $id){

        try {
            $user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {
                return $this->forward(
                    'App\Controller\DefaultController::update',
                    ['forward' => ['entity' => 'User', 'id' => $id, 'parameters' => $request->request->get('data') ] ]
                );
            }
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"} )
     */
    public function user_delete(Request $request, jwtAuth $jwtAuth, $id){

        try {
            $user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {
                return  $this->forward(
                    'App\Controller\DefaultController::delete',
                    ['forward' => ['entity' => 'User', 'id' => $id] ]
                );
            }
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }

    /**
     * @Route("", name="user_list", methods={"GET"} )
     */
    public function user_list(Request $request, Helpers $helpers, jwtAuth $jwtAuth) {

        try {
            /*$user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            if ($token['check']) {*/
                $data = json_decode( $this->forward(
                    'App\Controller\DefaultController::read',
                    ['forward' => ['entity' => 'User', 'parameters' => $request->query->all() ]]
                )->getContent(), 1);
                $users = $data['data'];
                foreach ($users as $key => &$user) {
                    $user['password'] = 'encripted';
                    $user['token'] = 'encripted';
                }
                $data['data'] = [$users];
                return $helpers->json($data);
            /*}
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);*/
        } catch (Exception $e) {
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage() , 'ok' => false], 500 );
        }
    }
}