<?php

namespace App\Entity;

use App\Repository\UserBikeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserBikeRepository::class)
 * @UniqueEntity( fields={"bike", "user", "rentStart", "returned"},
 *     errorPath="bike",
 *     message="this bike has already been rented or has already been returned."
 * )
 * 
 */
class UserBike{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Bike::class, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bike;

    /**
     * @ORM\Column(type="date")
     */
    private $rentStart;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $returnBike;

    /**
     * @ORM\Column(type="boolean")
     */
    private $returned = false;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $rentCost;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBike(): ?Bike
    {
        return $this->bike;
    }

    public function setBike(?Bike $bike): self
    {
        $this->bike = $bike;

        return $this;
    }

    public function getRentStart(): ?\DateTimeInterface
    {
        return $this->rentStart;
    }

    public function setRentStart(\DateTimeInterface $rentStart): self
    {
        $this->rentStart = $rentStart;

        return $this;
    }

    public function getReturnBike(): ?\DateTimeInterface
    {
        return $this->returnBike;
    }

    public function setReturnBike(?\DateTimeInterface $returnBike): self
    {
        $this->returnBike = $returnBike;

        return $this;
    }

    public function getRentCost(): ?string
    {
        return $this->rentCost;
    }

    public function setRentCost(?string $rentCost): self
    {
        $this->rentCost = $rentCost;

        return $this;
    }

    public function getReturned(): ?bool
    {
        return $this->returned;
    }

    public function setReturned(?bool $returned): self
    {
        $this->returned = $returned;

        return $this;
    }

    public function __toString() {
        return $this->name;
    }
}
