<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateControllerCommand extends Command{

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-controller';

    protected function configure(){

        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('Creates a new controller.')
        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command helps you create a quick controller.')
        ->addArgument('controllerName',InputArgument::OPTIONAL,'controller name');
    }

    protected function execute(InputInterface $input, OutputInterface $output){

        try {

            $name = $input->getArgument('controllerName');
            $camelCaseByUnderScoreName = strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/',"_$1", $name));

            if($name){
                // build file path
                $filePath = getcwd().'/src/Controller/'.ucfirst($name).'Controller.php';
                // write file content
                $data = "<?php\n\n";
                $data .= "namespace App\Controller;\n\n";
                $data .= "use App\Service\jwtAuth;\n";
                $data .= "use Symfony\Component\HttpFoundation\Request;\n";
                $data .= "use Symfony\Component\Routing\Annotation\Route;\n";
                $data .= "use Symfony\Component\HttpFoundation\JsonResponse;\n";
                $data .= "use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;\n\n";
                $data .= "/**\n";
                $data .= " * @Route(\"/".strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/',"/$1", $name))."\")\n";
                $data .= " */\n";
                $data .= "class ".ucfirst($name)."Controller extends AbstractController {\n";
                $data .= "\n";
                $data .= "    /**\n";
                $data .= "     * @Route(\"\", name=\"".$camelCaseByUnderScoreName."_create\", methods={\"POST\", \"OPTIONS\"} )\n";
                $data .= "     */\n";
                $data .= "    public function ".$camelCaseByUnderScoreName."_create(Request $"."request, jwtAuth $"."jwtAuth){\n";
                $data .= "\n";
                $data .= "        try {\n";
                $data .= "            /*$"."user = $"."this->getUser();\n";
                $data .= "            $"."token = $"."jwtAuth->checkToken($"."user);\n";
                $data .= "            if ($"."token['check']) {*/\n";
                $data .= "                return  $"."this->forward(\n";
                $data .= "                    'App\Controller\DefaultController::create',\n";
                $data .= "                    ['forward' => '{\"".ucfirst($name)."\":'.$"."request->request->get('data').'}' ]\n";
                $data .= "                );\n";
                $data .= "            /*}\n";
                $data .= "            return new JsonResponse(['message' => $"."token['message'], 'ok' => false], 403);*/\n";
                $data .= "        } catch (Exception $"."e) {\n";
                $data .= "            return new JsonResponse(['message' => 'Operation failed. ' . $"."ex->getMessage() , 'ok' => false], 500 );\n";
                $data .= "        }\n";
                $data .= "    }\n";
                $data .= "\n";
                $data .= "    /**\n";
                $data .= "     * @Route(\"/{id}\", name=\"".$camelCaseByUnderScoreName."_read\", methods={\"GET\"} )\n";
                $data .= "     */\n";
                $data .= "    public function ".$camelCaseByUnderScoreName."_read(Request $"."request, jwtAuth $"."jwtAuth, $"."id){\n";
                $data .= "\n";
                $data .= "        try {\n";
                $data .= "            /*$"."user = $"."this->getUser();\n";
                $data .= "            $"."token = $"."jwtAuth->checkToken($"."user);\n";
                $data .= "            if ($"."token['check']) {*/\n";
                $data .= "                return $"."this->forward(\n";
                $data .= "                    'App\Controller\DefaultController::read',\n";
                $data .= "                    ['forward' => ['entity' => '".ucfirst($name)."', 'parameters' => ['".$camelCaseByUnderScoreName."|id' => '=|'.$"."id] ] ]\n";
                $data .= "                );\n";
                $data .= "            /*}\n";
                $data .= "            return new JsonResponse(['message' => $"."token['message'], 'ok' => false], 403);*/\n";
                $data .= "        } catch (Exception $"."e) {\n";
                $data .= "            return new JsonResponse(['message' => 'Operation failed. ' . $"."ex->getMessage() , 'ok' => false], 500 );\n";
                $data .= "        }\n";
                $data .= "    }\n";
                $data .= "\n";
                $data .= "    /**\n";
                $data .= "     * @Route(\"/{id}\", name=\"".$camelCaseByUnderScoreName."_update\", methods={\"PUT\"} )\n";
                $data .= "     */\n";
                $data .= "    public function ".$camelCaseByUnderScoreName."_update(Request $"."request, jwtAuth $"."jwtAuth, $"."id){\n\n";
                $data .= "        try {\n";
                $data .= "            /*$"."user = $"."this->getUser();\n";
                $data .= "            $"."token = $"."jwtAuth->checkToken($"."user);\n";
                $data .= "            if ($"."token['check']) {*/\n";
                $data .= "                return $"."this->forward(\n";
                $data .= "                    'App\Controller\DefaultController::update',\n";
                $data .= "                    ['forward' => ['entity' => '".ucfirst($name)."', 'id' => $"."id, 'parameters' => $"."request->request->get('data') ] ]\n";
                $data .= "                );\n";
                $data .= "            /*}\n";
                $data .= "            return new JsonResponse(['message' => $"."token['message'], 'ok' => false], 403);*/\n";
                $data .= "        } catch (Exception $"."e) {\n";
                $data .= "            return new JsonResponse(['message' => 'Operation failed. ' . $"."ex->getMessage() , 'ok' => false], 500 );\n";
                $data .= "        }\n";
                $data .= "    }\n";
                $data .= "\n";
                $data .= "    /**\n";
                $data .= "     * @Route(\"/{id}\", name=\"".$camelCaseByUnderScoreName."_delete\", methods={\"DELETE\"} )\n";
                $data .= "     */\n";
                $data .= "    public function ".$camelCaseByUnderScoreName."_delete(Request $"."request, jwtAuth $"."jwtAuth, $"."id){\n\n";
                $data .= "        try {\n";
                $data .= "            /*$"."user = $"."this->getUser();\n";
                $data .= "            $"."token = $"."jwtAuth->checkToken($"."user);\n";
                $data .= "            if ($"."token['check']) {*/\n";
                $data .= "                return  $"."this->forward(\n";
                $data .= "                    'App\Controller\DefaultController::delete',\n";
                $data .= "                    ['forward' => ['entity' => '".ucfirst($name)."', 'id' => $"."id] ]\n";
                $data .= "                );\n";
                $data .= "            /*}\n";
                $data .= "            return new JsonResponse(['message' => $"."token['message'], 'ok' => false], 403);*/\n";
                $data .= "        } catch (Exception $"."e) {\n";
                $data .= "            return new JsonResponse(['message' => 'Operation failed. ' . $"."ex->getMessage() , 'ok' => false], 500 );\n";
                $data .= "        }\n";
                $data .= "    }\n\n";
                $data .= "    /**\n";
                $data .= "     * @Route(\"\", name=\"".$camelCaseByUnderScoreName."_list\", methods={\"GET\"} )\n";
                $data .= "     */\n";
                $data .= "    public function ".$camelCaseByUnderScoreName."_list(Request $"."request, jwtAuth $"."jwtAuth) {\n\n";

                $data .= "        try {\n";
                $data .= "            /*$"."user = $"."this->getUser();\n";
                $data .= "            $"."token = $"."jwtAuth->checkToken($"."user);\n";
                $data .= "            if ($"."token['check']) {*/\n";
                $data .= "                return  $"."this->forward(\n";
                $data .= "                    'App\Controller\DefaultController::read',\n";
                $data .= "                    ['forward' => ['entity' => '".ucfirst($name)."', 'parameters' => $"."request->query->all() ]]\n";
                $data .= "                );\n";
                $data .= "            /*}\n";
                $data .= "            return new JsonResponse(['message' => $"."token['message'], 'ok' => false], 403);*/\n";
                $data .= "        } catch (Exception $"."e) {\n";
                $data .= "            return new JsonResponse(['message' => 'Operation failed. ' . $"."ex->getMessage() , 'ok' => false], 500 );\n";
                $data .= "        }\n";
                $data .= "    }\n";
                $data .= "}";
                // using flag FILE_APPEND to add the content to file end
                // and flag LOCK_EX to prevent anyone from writing to the file at the same time
                file_put_contents($filePath, $data);
                //file_put_contents($filePath, $data, FILE_APPEND | LOCK_EX);
                $output->writeln("\"¡Success!\" ".ucfirst($name)." controller created.");
            }else{
                $output->writeln('');
                $output->writeln('You must enter the name of the entity that will have a direct relationship with the controller.');
                $output->writeln('Example: php bin/console app:create-controller Course');
                $output->writeln('');
            }
        } catch (\Throwable $e) {
            $output->writeln( $e->getMessage() );
        }
        return 1;
    }
}