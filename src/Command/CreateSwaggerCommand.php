<?php

namespace App\Command;

use Symfony\Component\Yaml\Yaml;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateSwaggerCommand extends Command{

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-swagger';
    private $titleDoc;
    private $hostName;
    private $hostProtocol;

    public function __construct(RouterInterface $router, EntityManagerInterface $entityManager){
        $this->router = $router;
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure(){

        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('Creates a new Swagger.')
        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command helps you create a quick Swagger.');
    }

    public function setHost($hostName, $hostProtocol, $titleDoc) {
        $this->titleDoc = $titleDoc;
        $this->hostName = $hostName;
        $this->hostProtocol = $hostProtocol;
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        try {
            // build file path
            $filePath = getcwd().'/templates/swagger_ui/swagger.yaml';
            // write file content
            $paths = [
                "openapi" => "3.0.0",
                "info" => [
                    "version" => "1.0.0",
                    "title" => $this->titleDoc,
                    "license" => [
                        "name" => "MIT"
                    ]
                ],
                "servers" => [
                    ["url" => $this->hostProtocol."://".$this->hostName]
                ],
                "schemes" => ["http", "https"]
            ];
            /* GET ALL ROUTES */
            $routes = $this->router->getRouteCollection()->all();
            $tags = [];
            /* Initializate routes */
            foreach ($routes as $key => $route) {
                if( !in_array($key, ['_preview_error', 'read', 'create', 'update', 'delete'], true) ){
                    $tag = explode('Controller', explode('App\Controller\\', $route->getDefaults()['_controller'] )[1])[0];
                    $path = $route->getPath();
                    $methods = $route->getMethods();
                    if( count($methods) > 0 ){
                        $pathVariables = $route->compile()->getVariables();
                        if(!isset($tags[$tag])){
                            $tags[$tag] = [ $path => [ ['method' => $methods[0], 'pathVariables' => $pathVariables] ] ];
                        }else{
                            if(!isset($tags[$tag][$path])){
                                $tags[$tag][$path] = [ ['method' => $methods[0], 'pathVariables' => $pathVariables] ];
                            }else{
                                $tags[$tag][$path] = array_merge( $tags[$tag][$path], [ ['method' => $methods[0], 'pathVariables' => $pathVariables] ] );
                            }
                        }
                    }
                }
            }
            /* GET CUSTOMS ENDPOINTS */
            $custom = Yaml::parseFile(getcwd().'/templates/swagger_ui/swaggerCustom.yaml');
            /* INITIALIZATE PATHS */
            $paths["paths"] = [];
            /* FETCH TAGS */
            foreach ($tags as $key => $tag) {
                /* get mapping of entities */
                $metaEntity = $fieldMappings = $this->entityManager->getClassMetadata("App\Entity\\".$key);
                $fieldMappings = $metaEntity->fieldMappings;
                $associationMappings = $metaEntity->associationMappings;
                /* get mapping of entities */
                foreach ($tag as $keyTag => $methods) {
                    foreach ($methods as $keyMethod => $method) {
                        $lcaseMethod = strtolower($method['method']);
                        // create route tag if no exit
                        if( !isset($paths["paths"][$keyTag]) ){
                            $paths["paths"][$keyTag] = [ $lcaseMethod => [] ];
                        }
                        /* IF CUSTOM METHODS ENDPOINT ADD IT */
                        if( isset($custom[$key][$keyTag][$lcaseMethod] ) ){
                            $cloneCustom = $custom[$key][$keyTag][$lcaseMethod];
                            $paths["paths"][$keyTag][$lcaseMethod] = $cloneCustom;
                        }else{
                            $paths["paths"][$keyTag][$lcaseMethod] = [
                                "security" => ["bearerAuth" => [] ],
                                "operationId" => $method['method']."_".$key."_".$keyMethod,
                                "consumes" => ["application/json"],
                                "produces" => ["application/json"],
                                "tags" => [$key]
                            ];
                            if(count($method['pathVariables']) > 0){
                                $paths["paths"][$keyTag][$lcaseMethod]["parameters"] = [];
                                foreach ( $method['pathVariables'] as $keyVars => $vars) {
                                    $newParam = [
                                        "name" => $vars,
                                        "in" => "path",
                                        "required" => "true"
                                    ];
                                    array_push($paths["paths"][$keyTag][$lcaseMethod]["parameters"], $newParam);
                                }
                            }
                            if($method['method'] == 'POST' || $method['method'] == 'PUT'){
                                /* Build example */
                                $example = '{';
                                $count = 0;
                                foreach ($fieldMappings as $keyMap => $fields) {
                                    if($fields['fieldName'] != 'id'){
                                        $example .= $count > 0 ? ', ': '';
                                        $example .= '"'.$fields['fieldName'].'":"type/'.$fields['type'].'"';
                                        $example .= isset($fields['length']) ? ' length/'.$fields['length'] : '';
                                        if($method['method'] == 'POST'){
                                            $example .= !$fields['nullable'] ? ' required"' : '"';
                                        }else{
                                            $example .= '"';
                                        }
                                        $count ++;
                                    }
                                }
                                foreach ($associationMappings as $keyMap => $fields) {
                                    if(isset($fields['joinColumnFieldNames']) ){
                                        $example .= $count > 0 ? ', ': '';
                                        $example .= '"'.$fields['fieldName'].'":"type/integer"';
                                        if($method['method'] == 'POST'){
                                            if( isset($fields['joinColumns'][0]) && isset($fields['joinColumns'][0]['nullable']) ){
                                                $example .= !$fields['joinColumns'][0]['nullable'] ? ' required"' : '"';
                                            }
                                        }else{
                                            $example .= '"';
                                        }
                                        $count ++;
                                    }
                                }
                                $example .= '}';
                                $paths["paths"][$keyTag][$lcaseMethod]["requestBody"] = [
                                    "required" => true,
                                    "content" => [
                                        "application/x-www-form-urlencoded" => [
                                            "schema" => [
                                                "type" => "object",
                                                "properties" => [
                                                    "data" => [
                                                        "description" => $example,
                                                        "type" => "json"
                                                    ]
                                                ],
                                                "required" => [
                                                    "data"
                                                ]
                                            ]
                                        ]
                                    ]
                                ];

                            }else{
                                if($method['method'] == 'GET'){
                                    if(!isset( $paths["paths"][$keyTag][$lcaseMethod]["parameters"] )){
                                        $paths["paths"][$keyTag][$lcaseMethod]["parameters"] = [];
                                    }
                                    /* set inputs page, limit by querystring */
                                    if(count($method['pathVariables']) == 0){
                                        $paths["paths"][$keyTag][$lcaseMethod]["parameters"] = [
                                            [
                                                "name" => "page",
                                                "in" => "query",
                                                "description" => "Page number want to return",
                                                "schema" => [
                                                    "type" => "integer"
                                                ]
                                            ],
                                            [
                                                "name" => "limit",
                                                "in" => "query",
                                                "description" => "How many items to return at one time",
                                                "schema" => [
                                                    "type" => "integer"
                                                ]
                                            ]
                                        ];
                                        // BUILD FIELDS MAPPINGS
                                        foreach ($fieldMappings as $keyMap => $fields) {
                                            $newField = [
                                                    "name" => $metaEntity->table['name']."|".$fields['fieldName'],
                                                    "in" => "query",
                                                    "description" => "filter by ".$fields['fieldName']." / condition|value. Example / like|alex",
                                                    "schema" => [
                                                        "type" => "string"
                                                    ]
                                            ];
                                            array_push($paths["paths"][$keyTag][$lcaseMethod]["parameters"], $newField);
                                        }
                                    }
                                    // BUILD ASSOCIATION MAPPINGS
                                    if(!count($method['pathVariables']) > 0){
                                        foreach ($associationMappings as $keyMap => $fields) {
                                            if(isset($fields['joinColumnFieldNames']) ){
                                                $newField = [
                                                    "name" => $metaEntity->table['name']."|".$fields['joinColumns'][0]['name'],
                                                    "in" => "query",
                                                    "description" => "filter by ".$fields['joinColumns'][0]['name']." / condition|value. Example / !=|3",
                                                    "schema" => [
                                                        "type" => "string"
                                                    ]
                                                ];
                                                array_push($paths["paths"][$keyTag][$lcaseMethod]["parameters"], $newField);
                                            }
                                        }
                                    }
                                }
                            }
                            $code = $lcaseMethod == 'post' ? '201' : '200';
                            $paths["paths"][$keyTag][$lcaseMethod]["responses"] = [
                                $code => [
                                    "description" => $key. " object",
                                    "content" => [
                                        "application/json" => [
                                            "schema" => [
                                                "properties" => [
                                                    "ok" => [
                                                        "type" => "boolean"
                                                    ],
                                                    "message" => [
                                                        "type" => "string"
                                                    ],
                                                    "data" => [
                                                        "$"."ref" => "#/components/schemas/".$key
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                "400" => [
                                    "description" => "Bad request",
                                ],
                                "403" => [
                                    "description" => "You are not authorized",
                                ],
                                "500" => [
                                    "description" => "Operation failed"
                                ]
                            ];
                        }
                    }
                }
            }
            // Build components schemas from all entities
            $namesEntities = [];
            $data = "components:\n";
            $data .= "  schemas:\n";
            foreach ($this->entityManager->getConfiguration()->getMetadataDriverImpl()->getAllClassNames() as $key => $value) {
                $metaEntity = $this->entityManager->getClassMetadata($value);
                $fieldMappings = $metaEntity->fieldMappings;
                $associationMappings = $metaEntity->associationMappings;
                $sourceName = explode("\\", $value)[2];
                $namesEntities[] = $sourceName ;
                $data .= "    ".$sourceName.":\n";
                $data .= "      type: object\n";
                $data .= "      properties:\n";
                foreach ($fieldMappings as $keyMap => $fields) {
                    $data .= "        ".$fields['fieldName'].":\n";
                    $data .= "          type: ".$fields['type']."\n";
                    $newField = [ $fields['fieldName'] => ["type" => $fields['type'] ] ];
                }
                foreach ($associationMappings as $keyMap => $fields) {
                    if(isset($fields['joinColumnFieldNames']) ){
                        $targetEntity = $fields['targetEntity'];
                        $data .= "        ".$fields['fieldName'].":\n";
                        $data .= "          $"."ref: \"#/components/schemas/".explode("\\", $targetEntity)[2]."\"\n";
                        $newField = [$fields['fieldName'] => ["$"."ref" => "#/components/schemas/".explode("\\", $targetEntity)[2] ] ];
                    }
                }
            }
            /* SET PATH TO FILE */
            $yaml = Yaml::dump($paths);
            file_put_contents($filePath, $yaml);
            /* set security */
            $data .= "  securitySchemes:\n";
            $data .= "    bearerAuth:\n";
            $data .= "      type: ".$this->hostProtocol."\n";
            $data .= "      scheme: bearer\n";
            $data .= "      bearerFormat: JWT\n";
            $data .= "security:\n";
            $data .= "  - bearerAuth: []";
            // using flag FILE_APPEND to add the content to file end
            // and flag LOCK_EX to prevent anyone from writing to the file at the same time
            file_put_contents($filePath, $data, FILE_APPEND | LOCK_EX);

            // RESET LOAD_ENTITY_SERVICE
            $data = "<?php\n";
            $data .= "\n";
            $data .= "namespace App\Service;\n";
            $data .= "\n";
            $data .= "/* Entities imported */\n";
            foreach ($namesEntities as $key => $entity) {
            $data .= "use App\Entity\\".$entity.";\n";
            }
            $data .= "/* Entities import */\n";
            $data .= "\n";
            $data .= "class LoadEntity {\n";
            $data .= "\n";
            $data .= "    public function loadEntity($"."entityName) {\n";
            $data .= "        switch ($"."entityName) {\n";
            foreach ($namesEntities as $key => $entity) {
            $data .= "            case '$entity':\n";
            $data .= "                $"."entityName = new $entity"."();\n";
            $data .= "                break;\n";
            }
            $data .= "            default: \n";
            $data .= "                throw new \Exception(\"Entity "."$"."entityName does not loaded in MysqlEasy.\");\n";
            $data .= "        }\n";
            $data .= "        return $"."entityName;\n";
            $data .= "    }\n";
            $data .= "\n";
            $data .= "}\n";
            // SAVE LOAD_ENTITY_SERVICE
            file_put_contents(getcwd().'/src/Service/LoadEntity.php', $data);
            $output->writeln("\"¡Success! Swagger created ok.");

        } catch (\Throwable $e) {
            $output->writeln( $e->getMessage() );
        }
        return 1;
    }
}