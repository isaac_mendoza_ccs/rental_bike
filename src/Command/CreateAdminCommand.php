<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateAdminCommand extends Command{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-admin'; // run: php bin/console app:create-admin your_email your_password

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure(){
        // the short description shown while running "php bin/console list"
        $this->setDescription('Creates a new user admin.')
        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command helps you generate a new user admin in your database.')
        ->addArgument('email',InputArgument::REQUIRED,'user email')
        ->addArgument('password',InputArgument::REQUIRED,'user password');
    }

    protected function execute(InputInterface $input, OutputInterface $output){

    	try {
	    	$em = $this->entityManager;
	        $email = $input->getArgument('email');
	        $password = password_hash( $input->getArgument('password'), PASSWORD_DEFAULT);
	        $emailExist = count($em->getRepository(User::class)->findByEmail( $email ));
		    $output->writeln('');
			if($emailExist == 0){
				// add User
	        	$user = new User();
	        	$user->setEmail($email);
	        	$user->setUsername($email);
	        	$user->setPassword($password);
	        	$em->persist($user);
	        	$em->flush();
		        $output->writeln('email: '.$email);
		        $output->writeln('password: '.$password );
		        $output->writeln('');
		        $output->writeln('Success. User admin created!' );
			}else{
		        $output->writeln('Failed. Email '.$email.' already exist!');
			}
		    $output->writeln('');
        } catch (\Throwable $e) {
        	$output->writeln('');
            $output->writeln( 'Error. '. $e->getMessage() );
            $output->writeln('');
            $output->writeln('Try: php bin/console app:create-admin your_email your_password');
            $output->writeln('');
        }
        return 1;
    }
}