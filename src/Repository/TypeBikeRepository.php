<?php

namespace App\Repository;

use App\Entity\TypeBike;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeBike|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeBike|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeBike[]    findAll()
 * @method TypeBike[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeBikeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeBike::class);
    }

    // /**
    //  * @return TypeBike[] Returns an array of TypeBike objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeBike
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
