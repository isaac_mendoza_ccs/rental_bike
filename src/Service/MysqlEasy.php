<?php

namespace App\Service;

use App\Service\LoadEntity;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

/* Entities imported */
use App\Entity\User;
use App\Entity\Book;
use App\Entity\Sport;
use App\Entity\CategoryBook;
/* Entities import */

class MysqlEasy {

    private $manager;
    private $validator;
    private $filesystem;
    private $projectDir;
    private $mysqlSchema;
    private $containerBag;
    private $entitySpaceName;

    public function __construct($manager, $validator, ContainerBagInterface $containerBag) {

        $this->manager = $manager;
        $this->validator = $validator;
        $this->containerBag = $containerBag;
        $this->filesystem = new Filesystem();
        $this->entitySpaceName = 'App\Entity\\';
        $this->projectDir = $this->containerBag->get('kernel.project_dir');

        if(!$this->filesystem->exists( $this->projectDir.'/var/cache/prod/MysqlSchema/file.yaml') ){
        	$entities = [];
			$metas = $this->manager->getMetadataFactory()->getAllMetadata();
            $this->filesystem->mkdir( $this->projectDir.'/var/cache/prod/MysqlSchema');
			foreach ($metas as $meta) {
				$properties = [];
				foreach ($meta->fieldMappings as $key => $field) {
					$properties[$key] = [
						"type" => $field['type'],
						"columnName" => $field['columnName']
					];
				}
				$entities[ explode('\\', $meta->name)[2] ] = [
					"type" => "object",
					"properties" => $properties
				];
			}
			$yaml = Yaml::dump($entities);
			file_put_contents($this->projectDir.'/var/cache/prod/MysqlSchema/file.yaml', $yaml);
        }
    	$this->mysqlSchema = Yaml::parse(file_get_contents(
    		$this->projectDir.'/var/cache/prod/MysqlSchema/file.yaml')
    	);
    }

    private function loadEntity($entityName) {
        $loadEntity = new LoadEntity();
        return $loadEntity->loadEntity($entityName);
    }

    public function insert($parameters, $ignored = [], $entity = null) {
        //throw new \Exception(json_encode($parameters).' aqui');
        $parameters = is_array($parameters) ? $parameters : json_decode($parameters, 1);
        $parameters = [ucfirst(array_key_first($parameters)) => $parameters[array_key_first($parameters)] ];
        // load entity class if not come 
        if(!$entity){  
        	$entity = isset( $parameters[array_key_first($parameters)]['id'] ) ?
        		$this->loadEntityById(array_key_first($parameters), $parameters[array_key_first($parameters)]['id']) 
        	:
        		$this->loadEntity(array_key_first($parameters));
        }
        if(!$entity){
            throw new \Exception(array_key_first($parameters).' with id '.$parameters[array_key_first($parameters)]['id'].' not found');
        }
    	return $this->setParametersToEntity($entity, $parameters, $ignored);
    }

    public function setParametersToEntity($entity, $parameters, $ignore = [], $deep = 0) {

    	if(!is_object($entity)){ throw new \Exception('The class '.$entity.' does not exists.!'); }
    	// get informations of entity class
    	$completeEntityName = get_class($entity);
    	$entityName = explode('\\', $completeEntityName)[2];
        $fieldMappings = $this->manager->getClassMetadata($completeEntityName)->fieldMappings;
        $associationMappings = $this->manager->getClassMetadata($completeEntityName)->associationMappings;
        // set properties to entity
        foreach ($fieldMappings as $mapping => $field) {
            $parEntName = $parameters[$entityName];
        	if($mapping != 'id' & isset( $parEntName[$field['columnName']] ) & !in_array($mapping, $ignore)  ){
            	$value = $this->formatValue( $parEntName[$field['columnName']], $field);
                // if password set hash
                if($mapping == 'password'){ $value = password_hash( $value, PASSWORD_DEFAULT); }
                // Build Method Set.
                $method = $this->buildMethodSet($mapping);
                $entity->$method($value);
        	}
        }
        // add association mappings
        foreach ($associationMappings as $mapping => $field) {
            $mappingLCF = lcfirst($mapping);
        	if($mappingLCF != 'id' & isset( $parEntName[$mappingLCF] ) & !in_array($mappingLCF, $ignore)  ){
        		$associationName = explode('\\', $field["targetEntity"])[2];					
        		// true property entity collection (childs)
        		if( is_array($parEntName[$mappingLCF]) ){
        			foreach ($parEntName[$mappingLCF] as $keyChild => $child) {
        				$childEntity = $this->setParametersToEntity($this->loadEntity($associationName), [$associationName => $child], [], $deep + 1 );
        				// Build Method Add (to add in entity father).
		                $method = $this->buildMethodAdd($mappingLCF);
		                $entity->$method($childEntity);
        			}
        		}else{ // false property father entity (fathers)
        			$fatherEntity = $this->loadEntityById($associationName, $parEntName[$mappingLCF]);
	                // Build Method Set.
	                $method = $this->buildMethodSet($mappingLCF);
	                $entity->$method($fatherEntity);
        		}
        	}
        }
        // save original entity (Father) on cascade.
        if($deep === 0){
        	$entity = $this->saveEntity($this->validateEntity($entity));
        }else{ // validate child entity before to save the father entity
        	$entity = $this->validateEntity($entity);
        }
        // return result
        return $entity;
    }

    private function formatValue($value, $field){
		switch ($field['type']) {
			case 'string':
				break;
			case 'integer':
				$value = (int) $value;
				break;
			case 'time':
                $value = explode(':', $value);
                $d = mktime($value[0], $value[1], $value[2]);
                $value = date("H:i:s", $d);
				break;
			case 'decimal':
                $value = $value == '' ? 0 : number_format($value);
				break;
			case 'date':
				$value = \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime(str_replace('-', '/', $value))));
				break;
			case 'boolean':
                $bolleanArray = ['0','1','true','false'];
				$value = in_array($value, $bolleanArray) ? $value : false;
				break;
			default:
                throw new \Exception('field type "'.$field['type'].'" is not set.!');
		}
    	return $value;
    }

    private function buildMethodSet($property) {
        $arr = explode("_", $property); // e.g. in = language_source
        foreach ($arr as $x => &$val) {
            $val = ucfirst($val);
        }
        $method = 'set' . join("", $arr); // e.g. out = setLanguageSource
        return $method;
    }

    private function buildMethodAdd($property) {
        $arr = explode("_", $property); // e.g. in = language_source
        foreach ($arr as $x => &$val) {
            $val = ucfirst($val);
        }
        $method = 'add' . join("", $arr); // e.g. out = setLanguageSource
        return substr($method, 0, -1);
    }

    public function loadEntityById($entityName, $entityId) {
    	return $this->manager->getRepository($this->entitySpaceName.$entityName)->findOneById($entityId);
    }

    public function saveEntity($entity) {
        $this->manager->persist($entity);
        $this->manager->flush();
        return $entity;
    }

    public function removeEntity($entity){
        $this->manager->remove($entity);
        $this->manager->flush();
        return null;
    }

    public function removeEntityByNameId($entityName, $entityId){
        $entity = $this->loadEntityById($entityName, $entityId);
        if($entity){
            $this->removeEntity($entity);
            return null;
        }
    }

    public function validateEntity($entity) {
        $errors = $this->validator->validate($entity);
        if (count($errors) > 0) {
            $arrayError = [];
            foreach ($errors as $x => $err) {
                $arrayError[$x] = ['message' => $err->getMessage(), 'value' => $err->getInvalidValue(), 'property' => $err->getPropertyPath()];
            }
            throw new \Exception($arrayError[0]['message'].' Value: '.$arrayError[0]['value'].'. Property: '.$arrayError[0]['property']);
        } else {
        	return $entity;
        }
    }

    /* SELECT SESSION */
    public function select($entityName, $parameters = [], $ignored = null) {

        if($ignored){        
            $ignored = explode(',', $ignored);
            foreach ($ignored as $key => &$field) {
                $field = str_replace("|",".",$field);
            }
        }
    	//$entityName = explode('Controller', explode('\\', get_class($controllerEntity) )[2])[0];
    	$meta = $this->manager->getClassMetadata($this->entitySpaceName.$entityName);

        $page = isset($parameters['page']) ? $parameters['page'] : 1;
        $limit = isset($parameters['limit']) ? $parameters['limit'] : 10;

        $sql = 'SELECT CONCAT(\'{\'';
    	$newSql = $this->buildSelect($meta, 0, $this->entitySpaceName.$entityName, null, $parameters, $ignored);
        $sql .= $newSql['sql'].',\'}\') AS result FROM '.$meta->table['name'].$newSql['leftJoin'];

        $infoPage = 'SELECT COUNT('.$meta->table['name'].'.id) AS total FROM '.$meta->table['name'].$newSql['leftJoin'];
        $infoPage .= $this->buildWhere($parameters);
        $infoPage = $this->calculateInfoPage($infoPage, ['page' => $page, 'limit' => $limit]);
        $sql .= $this->buildWhere($parameters).' LIMIT '.$limit.' OFFSET '.$infoPage['offset'];
        //dump($sql); dump($infoPage); die;
        // aplicate connection
    	$result = $this->aplicateConn($sql, $parameters, false);
        // decode json result
    	foreach ($result as $key => &$value) {
    		$value = json_decode( str_replace(',}', '}', $value['result']) ); // fix string if one property is ignored
            foreach ($value as $keyValue => &$val) {
                if($val == 'NULL'){
                    $val = null;
                }
            }
    	}
        //return result
        return ["total" => $infoPage['total'], "count" => count($result), "page" => $infoPage['page'], "pages" => $infoPage['totalPage'], "data" => $result];
    }

    private function buildWhere($parameters){
        $sql = '';
        $isWhere = 0;
        foreach ($parameters as $key => $param) {
            $keyVal = explode('|', $key);
            $operatorVal = explode('|', $param);
            if( isset($keyVal[1]) && isset($operatorVal[1]) ){            
                $sql .= $isWhere === 0 ? ' WHERE ' : ' AND ';
                if(strtolower($operatorVal[0]) === 'like'){
                    $sql .= $keyVal[0].'.'.$keyVal[1].' '.$operatorVal[0].' "%'.$operatorVal[1].'%"';
                }else{
                    $sql .= $keyVal[0].'.'.$keyVal[1].' '.$operatorVal[0].' "'.$operatorVal[1].'"';
                }
                if( isset($operatorVal[3]) ){  
                    if(strtolower($operatorVal[2]) === 'like'){
                        $sql .= ' OR '.$keyVal[0].'.'.$keyVal[1].' '.$operatorVal[2].' "%'.$operatorVal[3].'%"';
                    }else{
                        $sql .= ' OR '.$keyVal[0].'.'.$keyVal[1].' '.$operatorVal[2].' "'.$operatorVal[3].'"';
                    }
                }

                $isWhere++;
            }
        }
        return $sql;
    }

    private function buildSelect($meta, $numTable = 0, $father = null, $joinColumns = null, $parameters = [], $ignored = []) {

    	$originalNumTable = $numTable;
        $sql = '';
        $countKey = 1;
        // build select singles properties
        foreach ($meta->fieldMappings as $key => $field) {
            if( !in_array($meta->table['name'].'.'.$field['columnName'], $ignored) ){            
                $sql .= ',\'"'.$key.'":"\',IFNULL('.$meta->table['name'].'.'.$field['columnName'].', "NULL")';
                if( count($meta->fieldMappings) === $countKey){
                    $sql .= ',\'"\'';
                }else{
                    $sql .= ',\'",\'';
                }
                $countKey++;
            }
        }
        // build left join
        $leftJoin = '';
        // build select relationship properties
        foreach ($meta->associationMappings as $key => $field) {
            if($field['targetEntity'] !== $father){
                $numTable++;
                if( isset($field['joinColumns']) ){ // have father
                    $newMeta = $this->manager->getClassMetadata($field['targetEntity']);
                    $leftJoin = ' LEFT JOIN '.$newMeta->table['name'].' ON '.$newMeta->table['name'].'.id = '.$meta->table['name'].'.'.$field['joinColumns'][0]['name'];
                    $sql .= ',\',"'.$field['fieldName'].'":\',IFNULL((SELECT CONCAT(\'{\'';
                    $newSql = $this->buildSelect($newMeta, $numTable, $father, $field['joinColumns'][0]['name'], $parameters, $ignored ); // last to indicate getFather
                    $sql .= $newSql['sql'].',\'}\') FROM '.$newMeta->table['name'].' WHERE '.$newMeta->table['name'].'.id = '.$meta->table['name'].'.'.$field['joinColumns'][0]['name'].'), \'"null"\')';
                    $leftJoin .= $newSql['leftJoin'];
                }else{ // have collection
                    $newMeta = $this->manager->getClassMetadata($field['targetEntity']);
                    if( strlen($sql) === 0 ){
                        $sql .=  ',\'"'.$field['fieldName'].'":[\',IFNULL((SELECT GROUP_CONCAT(CONCAT(\'{\'';
                    }else{
                        $sql .=  ',\',"'.$field['fieldName'].'":[\',IFNULL((SELECT GROUP_CONCAT(CONCAT(\'{\'';
                    }
                    $newSql = $this->buildSelect($newMeta, $numTable, $father, null, [], $ignored);
                    $joinField = $newMeta->associationMappings[$field['mappedBy']]['targetToSourceKeyColumns']['id'];
                    $sql .= $newSql['sql'].',\'}\') SEPARATOR \',\') FROM '.$newMeta->table['name'].' WHERE '.$newMeta->table['name'].'.'.$joinField.' = '.$meta->table['name'].'.id ),\'\') ,\']\'';
                }
            }
        }
    	return ["sql" => $sql, "numTable" => $numTable, 'leftJoin' => $leftJoin];
    }

    public function calculateInfoPage($query, $queryString = [], $parameters = []){

        $total = (int)$this->aplicateConn($query, $parameters)[0]['total'];
        $limit = (array_key_exists('limit', $queryString) ? (int)$queryString['limit'] < 1 ? 10 : (int)$queryString['limit'] : 10);
        $page = (array_key_exists('page', $queryString) ? (int)$queryString['page'] < 1 ? 1 : (int)$queryString['page'] : 1);
        $totalPage = (int)ceil($total / $limit);
        $offset = ( ($limit * $page ) - $limit);
        return ['total' => $total, 'limit' => $limit, 'page' => $totalPage > 0 ? $page : 0, 'totalPage' => $totalPage, 'offset' => $offset];
    }

    public function aplicateConn($query, $parameters = [], $debug = false){ 
        // get mannager
        $conn = $this->manager->getConnection();
        if($debug){
            $stack = new \Doctrine\DBAL\Logging\DebugStack();
            $conn->getConfiguration()->setSQLLogger($stack);
        }
        $stmt = $conn->prepare($query);
        $stmt->execute($parameters);
        // dont remove the next line atte. ISAAC MENDOZA
        if($debug){ dump($stack->queries); die; }
        $result = substr($query, 0, 6); // UPDATE/DELETE
        return $result == "UPDATE" ? $stack : $result == "DELETE" ? $stack : $stmt->fetchAll();
    }

    public function buildFieldsString($queryString, $fieldAvailable) {
        // fields to select in sql
        $fieldString = '';
        // count index
        $index = 0;
        // filter by some properties
        if (array_key_exists('fields', $queryString)) { // must come in path eg. fields=id,username,lastname
            // Get fields to select
            $fields = explode(",", $queryString['fields']);
            // Get id by default
            array_unshift($fields, "id");
            // Loop fields to select
            foreach ($fields as $key => $field) {
                if (isset($fieldAvailable[$field])) {
                    $fieldString .= $index > 0 ? ', ' : '';
                    $fieldString .= $fieldAvailable[$field];
                    $index++;
                }
            }
        } else { // get all properties availables
            // Loop fields to select
            foreach ($fieldAvailable as $key => $field) {
                $fieldString .= $index > 0 ? ', ' : '';
                $fieldString .= $fieldAvailable[$key];
                $index++;
            }
        }
        return $fieldString;
    }
}