<?php

namespace App\Service;

/* Entities imported */
use App\Entity\Bike;
use App\Entity\TypeBike;
use App\Entity\UserBike;
use App\Entity\User;
/* Entities import */

class LoadEntity {

    public function loadEntity($entityName) {
        switch ($entityName) {
            case 'Bike':
                $entityName = new Bike();
                break;
            case 'TypeBike':
                $entityName = new TypeBike();
                break;
            case 'UserBike':
                $entityName = new UserBike();
                break;
            case 'User':
                $entityName = new User();
                break;
            default: 
                throw new \Exception("Entity $entityName does not loaded in MysqlEasy.");
        }
        return $entityName;
    }

}
