# Rental Bike

api management.

## Install php dependencies

sudo apt install php7.2-mbstring php7.2-xml php7.2-zip php7.2-mysql php7.2-curl

## Installation

Use the package manager composer.

```bash
composer install
```

## preconfiguration ##

note: in to mysql and execute --> CREATE DATABASE rental_bike CHARACTER SET utf8 COLLATE utf8_general_ci;

1) Leave the DB at zero.
2) Run doctrine migration.
```bash
php bin/console doctrine:migration:migrate
```
3) Working locally, don't forget to add the following line to the virtual host or your custom virtual host.
```bash
SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
```

## permissions to user groups and apache on a directory ##

```bash
sudo chown -R www-data:www-data /var/www/html/project_dir
sudo chmod -R g+rw /var/www/html/project_dir
```

## test start up - open browser and set
http://localhost/rental_bike/public/index.php/api/doc

## start up data
- Create your admin user.
```bash
php bin/console app:create-admin your_email your_password
```

- create an new user, in tag User method POST set body data:
```json
{"name":"Pedro", "lastname":"Perez", "email":"pedro.perez@rentalbike.com", "username":"pedro.perez@rentalbike.com", "password":"12345678"}
```

- create an new bike, in tag Bike method POST set body data:
```json
{"code":"007", "details":"perfect conditions", "typeBike":1}
```

- rent a bike, in tag UserBike /user/bike/rent set body data
```json
 {"user":1, "bike":1}
```

- return a rent bike, in tag UserBike /user/bike/{id}/rent/return set body data
```json
 {"return_bike":"2022-01-13"}
```

## useful commands ##

- create a new entity.
```bash
php bin/console make:entity Example
```

- create a new controller.
```bash
php bin/console app:create-controller EntityName
```